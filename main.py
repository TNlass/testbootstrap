# python program for flask and bootstrap

from flask import Flask, redirect, url_for, render_template
import os

IMG_FOLDER = os.path.join('static', 'IMG')

app = Flask(
    __name__,
    template_folder='templates',
    static_folder='static'
    )

app.config['UPLOAD_FOLDER'] = IMG_FOLDER

@app.route("/")
@app.route("/home")
def home():
    return render_template("index.html")

@app.route("/hello")
def hello():
    return render_template("hello.html")

@app.route("/poppies")
def poppies():
    poppies_icon = os.path.join(app.config['UPLOAD_FOLDER'], 'small_poppies.jpg')
    return render_template("poppies.html", user_image=poppies_icon)

@app.route("/about")
def about():
    about_icon = os.path.join(app.config['UPLOAD_FOLDER'], 'PythonLounge300.png')
    return render_template("about.html", user_image=about_icon)

if __name__ == "__main__":
    app.run(debug=True)
